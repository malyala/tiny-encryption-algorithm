
/***********************************************************
* Name: std_types.h                                        *
* Author: Amit Malyala                                     *
* Description: Custom data types for C/C++ programs        *
***********************************************************/
#ifndef STD_TYPES_H
#define STD_TYPES_H

/*--------------------- Typedefs--------------------------*/
typedef unsigned char BOOL;	   /* UINT8 8 bits */
typedef int SINT32; 		   /* signed int 32 bits */
typedef unsigned int UINT32;   /* usnigned int 32 bits */
typedef unsigned short UINT16; /* unsigned short int 16 bits */
typedef unsigned long UINT64;  /* unsigned long */
typedef short SINT16; 		   /* signed short int 16 bits */
typedef unsigned char UINT8;   /* UINT8 8 bits */
typedef char SINT8;    	       /* signed char 8 bits */
typedef double FLOAT64;		   /* Floating point FLOAT64 64 bits */
typedef float  FLOAT32;		   /* Floating point float 32 bits */

typedef volatile int VSINT32; 		     /* signed int 32 bits */
typedef volatile UINT32 VUINT32;   /* usnigned int 32 bits */
typedef volatile unsigned short VUINT16; /* unsigned short int 16 bits */
typedef volatile short VSINT16; 		 /* signed short int 16 bits */
typedef volatile unsigned long VUINT64;  /* unsigned long */
typedef volatile UINT8 VUINT8;   /* UINT8 8 bits */
typedef volatile char VSINT8;    	     /* signed char 8 bits */
typedef volatile double VFLOAT64;		 /* Floating point FLOAT64 64 bits */
typedef volatile float  VFLOAT32;		 /* Floating point float 32 bits */

#ifndef TRUE
#define TRUE  1
#endif

#ifndef FALSE
#define FALSE 0
#endif


#endif /* #ifndef STD_TYPES_H */
