# README #

This algorithm called **Tiny encryption algorithm**.


The Tiny Encryption Algorithm is one of the fastest and most efficient cryptographic algorithms in existence. 
It was developed by David Wheeler and Roger Needham at the Computer Laboratory of Cambridge University. It is a Feistel cipher which uses 
operations from mixed (orthogonal) algebraic groups - XOR, ADD and SHIFT in this case. This is a very clever way of providing Shannon's twin 
properties of diffusion and confusion which are necessary for a secure block cipher, without the explicit need for P-boxes and S-boxes respectively.
It encrypts 64 data bits at a time using a 128-bit key. It seems highly resistant to differential cryptanalysis, and achieves complete diffusion 
(where a one bit difference in the plaintext will cause approximately 32 bit differences in the ciphertext) after only six rounds. Performance on a 
modern desktop computer or workstation is very impressive. 

There's also a paper on  extended variants of TEA which addresses a couple of minor weaknesses 
(irrelevant in almost all real-world applications), and introduces a block variant of the algorithm which can be even faster in some circumstances. 
How secure is TEA?
Very. There have been no known successful cryptoanalyses of TEA.If you want a low-overhead end-to-end cipher (for real-time data, for example), 
then TEA fits the bill. 

It would be interesting to see if an implementation using the new Velocity Engine (Altivec) technology could be even more efficient. 
Without looking at this in any detail, I'd surmise that some of the ultra-wide (i.e. 16-byte) arithmetic instructions could yield a 
substantial performance enhancement, at least in ECB mode. 

##Notes##

TEA takes 64 bits of data in v[0] and v[1], and 128 bits of key in k[0] - k[3]. The result is returned in w[0] and w[1]. 
Returning the result separately makes implementation of cipher modes other than Electronic Code Book a little bit easier.
TEA can be operated in any of the modes of DES.n is the number of iterations. 32 is ample, 16 is sufficient, as few as eight 
should be OK for most applications, especially ones where the data age quickly (real-time video, for example). The algorithm achieves 
good dispersion after six iterations. The iteration count can be made variable if required.
Note this algorithm is optimised for 32-bit CPUs with fast shift capabilities. It can very easily be ported to assembly language on most CPUs.
delta is chosen to be the Golden ratio ((5/4)1/2 - 1/2 ~ 0.618034) multiplied by 232. On entry to decipher(), sum is set to be delta * n. 
Which way round you call the functions is arbitrary: DK(EK(P)) = EK(DK(P)) where EK and DK are encryption and decryption under key K respectively. 

Original location:
http://143.53.36.235:8080/tea.htm ( link doesn't work)


## Author's notes ##
The algorithm was changed to add more shifting and adding to obtain a cipher with much more deviation from original message
compared to the version of original authors. In this project, the TEA algorithm uses a text file for encryption 
and prints the dectypted message back. In your application, you can use the algorithm on realtime data 
which you are sending over a network or for audio and video streams. For encryption of audio and video streams,
a few modifications of encyrption and decryption to decrease encryption strength may be required depending on your 
CPU load.