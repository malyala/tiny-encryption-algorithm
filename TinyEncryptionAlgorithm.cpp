/*
Name :  TinyEncryptionAlgorithm.cpp
Authors: David Wheeler 
         Roger Needham 
		 Simon Shepherd
Modified by:
         Amit Malyala

		   
		   
Date : 24-05-2017
Description:

The Tiny Encryption Algorithm is one of the fastest and most efficient cryptographic algorithms in existence. 
It was developed by David Wheeler and Roger Needham at the Computer Laboratory of Cambridge University. It is a Feistel cipher which uses 
operations from mixed (orthogonal) algebraic groups - XOR, ADD and SHIFT in this case. This is a very clever way of providing Shannon's twin 
properties of diffusion and confusion which are necessary for a secure block cipher, without the explicit need for P-boxes and S-boxes respectively.
It encrypts 64 data bits at a time using a 128-bit key. It seems highly resistant to differential cryptanalysis, and achieves complete diffusion 
(where a one bit difference in the plaintext will cause approximately 32 bit differences in the ciphertext) after only six rounds. Performance on a 
modern desktop computer or workstation is very impressive. 

There's also a paper on  extended variants of TEA which addresses a couple of minor weaknesses 
(irrelevant in almost all real-world applications), and introduces a block variant of the algorithm which can be even faster in some circumstances. 
How secure is TEA?
Very. There have been no known successful cryptoanalyses of TEA.If you want a low-overhead end-to-end cipher (for real-time data, for example), 
then TEA fits the bill. 

It would be interesting to see if an implementation using the new Velocity Engine (Altivec) technology could be even more efficient. 
Without looking at this in any detail, I'd surmise that some of the ultra-wide (i.e. 16-byte) arithmetic instructions could yield a 
substantial performance enhancement, at least in ECB mode. 

Notes

TEA takes 64 bits of data in v[0] and v[1], and 128 bits of key in k[0] - k[3]. The result is returned in w[0] and w[1]. 
Returning the result separately makes implementation of cipher modes other than Electronic Code Book a little bit easier.
TEA can be operated in any of the modes of DES.n is the number of iterations. 32 is ample, 16 is sufficient, as few as eight 
should be OK for most applications, especially ones where the data age quickly (real-time video, for example). The algorithm achieves 
good dispersion after six iterations. The iteration count can be made variable if required.
Note this algorithm is optimised for 32-bit CPUs with fast shift capabilities. It can very easily be ported to assembly language on most CPUs.
delta is chosen to be the Golden ratio ((5/4)1/2 - 1/2 ~ 0.618034) multiplied by 232. On entry to decipher(), sum is set to be delta * n. 
Which way round you call the functions is arbitrary: DK(EK(P)) = EK(DK(P)) where EK and DK are encryption and decryption under key K respectively. 

http://143.53.36.235:8080/tea.htm  (link doesn't work)

Notes:
This algorithm would read english text from a input file, encrypt and store it in a string.
Then the algorithm would decrypt the string and print output. This algorithm can be used for encryption on realtime audio or video. 
For audio or video, make number of iternations,n equal to 4 or 8.

This encryption algorithm is pretty fast and is ideal for sending data over a network or for video or audio streaming.
It is easily portable. 

To do:
Make the algorithm portable on amd, intel platforms.
Known issues:
None
Unknown issues;
None
*/

#include <iostream>
#include <string>
#include <fstream>
#include "std_types.h"
#include "12-TinyEncryptionAlgorithm.h"


/*
Function Name : main()
Date : 17-05-2016
Description:
This function executes the control program.
Pre conditions: None
Post conditions: must return 0
*/
SINT32 main(void)
{
	TextAnalysis();
	return 0;
}

/*
Function Name :  void encipher(std::string &OriginalFile , std::string& EncryptedFile)
Date : 25-05-2017
Description:
This function encrypts a string.
Arguments: std::string &OriginalFile , std::string& EncryptedFile
Pre conditions:Input must be supplied as a std::string.Number of iterations,n  must be same in encipher and decipher.
               std::string must be portable in G++ and other compilers.
Post conditions: None
*/
void encipher(std::string &OriginalFile , std::string& EncryptedFile)
{
   register UINT64 sum=0,delta=0x9E3779B9;
   /*
   register unsigned long a=k[0],b=k[1],c=k[2],d=k[3];
   a, b, c, d should be modified. 
   These are pointer locations of the key.
   */
   register UINT64 a=0xE8,b=0x1C,c=0xa7,d=0x9B;
   UINT32 n=0;
   register UINT8 y=' ',z=' ';
   UINT32 NumberofCharactersRead =0;
   UINT32 NumberofCharactersinFile=OriginalFile.size();
   /*
   std::cout << "Number of chars in original file " << NumberofCharsinFile << std::endl;
   */
   /* code to be modified begin */
   while (NumberofCharactersRead < NumberofCharactersinFile)
   {
   	  y= OriginalFile[NumberofCharactersRead];
   	  NumberofCharactersRead++;
   	  z= OriginalFile[NumberofCharactersRead];
   	  NumberofCharactersRead++;
   	  n=32;
   	  sum=0;
   	  /*
   	  std::cout << "y is " << y << std::endl;
   	  std::cout << "z is " << z << std::endl;
   	  system("pause");
   	  */
   	  while (n>0)
   	  {
   	  	sum+= delta;
	    y += (z << 4)+ (a ^ z) + (sum ^ (z >> 5)) + b;
		z += (y << 4)+ (c ^ y )+ (sum ^ (y >> 5)) + d;
		n--; 	  	
	  }
	  EncryptedFile += y;
	  EncryptedFile += z;
   }
   /* code to be modified end */
}

/*
Function Name : void decipher(std::string &EncryptedFile , std::string& DecryptedFile)
Description:
This function decrypts a string.
Arguments: std::string &EncryptedFile , std::string& DecryptedFile
Pre conditions: input must be supplied as a std::string. Number of iterations,n must be same in encipher and decipher.
                std::string must be portable in G++ and other compilers.
Post conditions: Output stored in the string Decryptedfile.
*/
void decipher(std::string &EncryptedFile , std::string& DecryptedFile)
{
   register UINT64 sum=0,delta=0x9E3779B9;
   
   register UINT64 a=0xE8,b=0x1C,c=0xa7,d=0x9B;
   UINT32 n=0;
   register UINT8  y=' ',z=' ';	
   UINT32 NumberofCharactersRead =0;
   UINT32 NumberofCharactersinFile=EncryptedFile.size();
   /*
   std::cout << "Number of chars in encrypted file " << NumberofCharsinFile << std::endl;
   */
   /* code to be modified begin */
   while (NumberofCharactersRead < NumberofCharactersinFile)
   {
   	 y = EncryptedFile[NumberofCharactersRead];
   	 NumberofCharactersRead++;
   	 z= EncryptedFile[NumberofCharactersRead];
   	 NumberofCharactersRead++;
   	 n=32;
   	 sum = delta * n;
   	 
   	 while (n >0 )
   	 {
   	 	z -= (y << 4) + (c^ y) + (sum ^ ( y >> 5 ) )+ d;
   	 	y -= (z << 4 )+ (a^ z) + (sum ^ ( z >> 5 )) + b;
   	 	sum -= delta;
   	 	n--;
	 }
	 
	 /*
   	 std::cout << "y is " << y << std::endl;
   	 std::cout << "z is " << z << std::endl;
   	 system("pause");
   	 */
   	 
	 DecryptedFile += y;
	 DecryptedFile += z;
	 
   }
   /* code to be modified end */
}

/*
Function Name : void readFile(const std::string& Filename, std::string& OriginalFile)
Description:
Read and display contents of a filename
From cplusplus.com.
Arguments: Input file name 
Preconditions: The function should read a file and add each character into a string.If there are errors, an error message should be displayed.
Post conditions: Print error if file not found
*/
void readFile(const std::string& Filename, std::string& OriginalFile)
{
  //std::string line;
  
  std::ifstream myfile (Filename);
  SINT8 c;
  if (myfile.is_open())
  {
    while (myfile.get(c ))  // For reading bytes
    //while (getline(myfile,line ))
    {
       //std::cout << c ; // Printing bytes
      //std::cout << line << std::endl ; // Print a line
      OriginalFile +=c;
      //OriginalFile +=line;
    }
    myfile.close();
  }
  else
  {
      std::cout << std::endl << "File not found";
  }
}
/*
Function Name : void PrintFile(std::string& Filename)
Description:
Read and display contents of a filename
From cplusplus.com.
Arguments: Input file name 
Preconditions: The function should read a file and print each character. If there are errors, an error message should be displayed.
Post conditions: 
*/
void PrintFile(std::string& Filename)
{
  UINT32 NumberofCharsRead =0;
  UINT32 NumberofCharsinString = Filename.size();
  while (NumberofCharsRead  < NumberofCharsinString)
   {
   	  std::cout << Filename[NumberofCharsRead];
	  NumberofCharsRead++;   	
   }
}


/*
Function Name :  void TextAnalysis(void)
Description:
Decrypts an encrypted filename.
Arguments: Input file name 
Preconditions: The function should read a file and add each character into a string.If there are errors, an error message should be displayed. 
               This function calls other functions to encrypt and decrypt.
Post conditions: Print error if file not found
*/
void TextAnalysis(void)
{
	std::string EncryptedFile;
	std::string DecryptedFile;
	std::string OriginalFile;
	std::string Filename="File.txt";
    readFile(Filename,OriginalFile);
    std::cout << "Printing original file " << std::endl;
    PrintFile(OriginalFile);
    std::cout << "Encrypting original file " << std::endl;
    encipher(OriginalFile,EncryptedFile);
    std::cout << "Printing encrypted file " << std::endl;
    PrintFile(EncryptedFile);
    std::cout << "Decrypting original file " << std::endl;
    decipher(EncryptedFile,DecryptedFile);
    std::cout << std::endl << "Printing decrypted file " << std::endl;
    PrintFile(DecryptedFile);
}
